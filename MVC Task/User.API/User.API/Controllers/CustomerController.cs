﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using User.API.Validation;
using User.DAL.Helpers;
using User.DAL.Models;
using User.DAL.Operations;
using UsersMVC.DAL.Validation;
using ActionResult = User.DAL.Helpers.ActionResult;

namespace UsersMVC.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        [HttpPost("SaveCustomers")]
        public ActionResult SaveCustomers(Customer customer)
        {
            return CustomerOperation.SaveCustomer(customer);
        }

        [HttpPost("GetCustomers")]
        public Customer GetCustomers(Customer customer)
        {
            return CustomerOperation.GetCustomer(customer);
        }

        [HttpPut("UpdateCustomers")]
        public ActionResult UpdateCustomers(Customer customer)
        {
            return CustomerOperation.UpdateCustomer(customer);
        }

        [HttpDelete("DeleteCustomers")]
        public ActionResult DeleteCustomers(Customer customer)
        {
            ActionResult result = new ActionResult();
            result = CustomerValidation.ValidationDeleteCustomer(customer);

            if (result.Result == ResultCode.Faild)
                return result;

            return CustomerOperation.DeleteCustomer(customer);
        }


        [HttpPost("SaveCustomerDetails")]
        public ActionResult SaveCustomerDetails(CustomerDetail customerDetail)
        {
            return CustomerDetailOperation.SaveCustomerDetail(customerDetail);
        }

        [HttpGet("GetCustomerDetails")]
        public List<CustomerDetail> GetCustomerDetails(CustomerDetail customerDetail)
        {
            return CustomerDetailOperation.GetCustomerDetail(customerDetail);
        }


        [HttpPut("UpdateCustomerDetails")]
        public ActionResult UpdateCustomerDetails(CustomerDetail customerDetail)
        {
            return CustomerDetailOperation.UpdateCustomerDetail(customerDetail);
        }

        [HttpDelete("DeleteCustomerDetails")]
        public ActionResult DeleteCustomerDetails(CustomerDetail customerDetail)
        {
            ActionResult result = new ActionResult();
            result = CustomerDetailValidation.ValidationDeleteCustomerDetail(customerDetail);

            if (result.Result == ResultCode.Faild)
                return result;
            
            return CustomerDetailOperation.DeleteCustomerDetail(customerDetail);
        }

    }
}
