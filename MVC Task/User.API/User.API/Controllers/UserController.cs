﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using User.DAL.Models;
using User.DAL.Operations;
using ActionResult = User.DAL.Helpers.ActionResult;

namespace User.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpPost("Login")]
        public ActionResult Login(CustomerDetail user)
        {
            return UserOperation.Login(user);
        }

        [HttpPost("Search")]
        public List<Customer> Search(Customer user)
        {
            return UserOperation.Search(user);
        }
    }
}
