﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace User.DAL.Infrastructure.ModelAbstraction
{
    public class ModelBaseWithId
    {
        public int Id { get; set; }
    }
}
