﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using User.DAL.Helpers;
using User.DAL.Models;

namespace User.DAL.Operations
{
    public class CustomerDetailOperation
    {
        public static List<CustomerDetail> GetCustomerDetail(CustomerDetail customerDetail)
        {
            List<CustomerDetail> result = new List<CustomerDetail>();
            DataTable dt = new DataTable();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(@"Data Source=LAPTOP-ST5MDDKK\SQLEXPRESS;Initial Catalog=UsersDB;Integrated Security=True"))//"DefaultConnection"))//ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString())
            {
                string InsertQuery = @"SELECT * FROM CustomerDetails WHERE Id = CASE WHEN @id = 0 THEN Id ELSE @id END";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();

                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = customerDetail.Id;

                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        dt.Load(dr);

                        foreach (DataRow item in dt.Rows)
                        {
                            CustomerDetail personDetail1 = new CustomerDetail();

                            //person1.Id = item["Id"].ToString();
                            personDetail1.CustomerId = Convert.ToInt32(item["CustomerId"]);
                            personDetail1.UserPassword = item["UserPassword"].ToString();
                            personDetail1.RegisterDate = Convert.ToDateTime(item["RegisterDate"]);

                            result.Add(personDetail1);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        public static ActionResult SaveCustomerDetail(CustomerDetail customerDetail)
        {
            ActionResult result = new ActionResult();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(@"Data Source=LAPTOP-ST5MDDKK\SQLEXPRESS;Initial Catalog=UsersDB;Integrated Security=True"))
            {
                string InsertQuery = @"INSERT INTO CustomerDetails(CustomerId,UserPassword,RegisterDate)
                                       VALUES (@customerId,@userPassword,@registerDate)";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.Add("@customerId", System.Data.SqlDbType.Int).Value = customerDetail.CustomerId;
                    cmd.Parameters.Add("@userPassword", System.Data.SqlDbType.NVarChar).Value = customerDetail.UserPassword;
                    cmd.Parameters.Add("@registerDate", System.Data.SqlDbType.DateTime).Value = DateTime.Now;
                    

                    try
                    {
                        cmd.ExecuteNonQuery();
                        result.Result = ResultCode.Done;
                    }
                    catch (Exception ex)
                    {
                        result.Result = ResultCode.Faild;
                        result.Description = ex.StackTrace;
                    }
                }
            }

            return result;
        }


        public static ActionResult UpdateCustomerDetail(CustomerDetail customerDetail)
        {
            ActionResult result = new ActionResult();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(@"Data Source=LAPTOP-ST5MDDKK\SQLEXPRESS;Initial Catalog=UsersDB;Integrated Security=True"))
            {
                string InsertQuery = @"UPDATE CustomerDetails SET ";

                using (SqlCommand cmd = new SqlCommand())
                {
                    /*if (customerDetail.CustomerId != 0)
                    {
                        InsertQuery += customerDetail.CustomerId == null ? "" : ", " +
                            "CustomerId = @customerId";
                        cmd.Parameters.Add("@customerId", System.Data.SqlDbType.NVarChar, 50).Value = customerDetail.CustomerId;
                    }*/

                    if (!string.IsNullOrEmpty(customerDetail.UserPassword))
                    {
                        InsertQuery += customerDetail.UserPassword == null ? "" : " " +
                            "UserPassword = @userPassword";
                        cmd.Parameters.Add("@userPassword", System.Data.SqlDbType.NVarChar, 50).Value = customerDetail.UserPassword;
                    }


                    /*if (customerDetail.RegisterDate != null)
                    {
                        InsertQuery += customerDetail == null ? "" : ", " +
                            "UserPassword = @userPassword";
                        cmd.Parameters.Add("@userPassword", System.Data.SqlDbType.NVarChar, 50).Value = customerDetail.UserPassword;
                    }*/

                    cmd.CommandText = InsertQuery + " where CustomerId = @customerId";
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@customerId", System.Data.SqlDbType.Int).Value = customerDetail.CustomerId;


                    try
                    {
                        cmd.ExecuteNonQuery();
                        result.Result = ResultCode.Done;
                    }
                    catch (Exception ex)
                    {
                        result.Result = ResultCode.Faild;
                        result.Description = ex.StackTrace;
                    }
                }
            }

            return result;
        }

        public static ActionResult DeleteCustomerDetail(CustomerDetail customerDetail)
        {
            ActionResult result = new ActionResult();

            //make sure that connection string is valid
            using (SqlConnection conn = new SqlConnection(@"Data Source=LAPTOP-ST5MDDKK\SQLEXPRESS;Initial Catalog=UsersDB;Integrated Security=True"))
            {
                string InsertQuery = @"DELETE FROM CustomerDetails WHERE Id = CASE WHEN @id = 0 THEN Id ELSE @id END";

                using (SqlCommand cmd = new SqlCommand(InsertQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = customerDetail.CustomerId;

                    try
                    {
                        cmd.ExecuteNonQuery();
                        result.Result = ResultCode.Done;
                    }
                    catch (Exception ex)
                    {
                        result.Result = ResultCode.Faild;
                        result.Description = ex.StackTrace;
                    }
                }
            }

            return result;
        }
    }
}
