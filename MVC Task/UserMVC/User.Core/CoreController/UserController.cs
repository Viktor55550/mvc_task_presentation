﻿using Nancy.Json;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using User.Core.Helper;
using ActionResult = User.Core.Helper.ActionResult;
using System.Threading.Tasks;
using User.Core.Model;
using User.Core.ConnectingApi;

namespace User.Core.CoreController
{
    public class UserController
    {

        public static ActionResult LoginUserAsync(Users login)
        {
            ActionResult result = new ActionResult();

            string loginResultString = ConnectApi.CallApiMethod(login, "User/Login");

            JavaScriptSerializer js = new JavaScriptSerializer();

            dynamic blogObject = js.Deserialize<dynamic>(loginResultString);

            result.Result = (ResultCode)Convert.ToInt32(blogObject["result"]);
            result.Description = blogObject["Description"];

            return result;
        }

    }
}
