﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using User.Core.ConnectingApi;
using User.Core.Model;
using User.Core.CoreController;

namespace UserMVC.Controllers
{
    public class CustomerController : Controller
    {
        public IActionResult CustomerSearch()
        {
            return View();
        }

        public IActionResult MainCustomer()
        {
            return View();
        }

        public IActionResult AddCustomer()
        {
            return View();
        }

        [HttpPost]
        public ViewResult CustomerSearch(SearchCustomer customer)
        {
            SearchCustomer customerResult = new SearchCustomer();
            customerResult = CoreCustomerController.Search(customer);

            if (customerResult.responses.Count != 0)
                return View(customerResult);
            else
            {
                ViewBag.NoneCustomer = "Customer not found";
                return View();
            }
        }

        [HttpPost]
        public ViewResult GetAllInfo(SearchCustomer customer)
        {
            customer.Id = 1;
            SearchCustomer customerResult = new SearchCustomer();
            customerResult = CoreCustomerController.GetCutomerById(customer);

            return View("MainCustomer", customerResult);
        }

        [HttpPost]
        public ViewResult SaveCustomer(SearchCustomer customer)
        {

            if (ModelState.IsValid)
            {
                var x = CoreCustomerController.SaveCustomer(customer);

                if ((int)x.Result == 1)
                {
                    return GetAllInfo(customer);
                }
                else
                {
                    ViewBag.ErrorMessage = x.Description;
                    return View("Login");
                }
            }
            else
            {
                return View("AddCustomer");
            }
        }

    }
}
